const instadb = require('./instadb/instadb.service.js');
const print = require('./print/print.service.js');
// eslint-disable-next-line no-unused-vars



module.exports = function (app) {
  app.configure(instadb);
  app.configure(print);
};
