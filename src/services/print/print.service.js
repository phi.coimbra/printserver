// Initializes the `print` service on path `/print`
const createService = require('./print.class.js');
const hooks = require('./print.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/print', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('print');

  service.hooks(hooks);
};
