/* eslint-disable no-unused-vars */
//import {sendPrintData} from 'brother-print-node';
const {sendPrintData} = require('brother-print-node');

class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    
    return [];
  }

  async get (id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create (data, params) {
    /*if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;*/
   // console.log(data, params.query.nome)

    return new Promise((resolve, reject) => {
     
      switch(params.query.impressora){
        case '1' :
          console.log('AQUI')
            sendPrintData([params.query.nome, params.query.cargo], ['local', 'Brother QL-801']) 
        break

        case '2' :
            sendPrintData([params.query.nome, params.query.cargo], ['local', 'Brother QL-802']) 
        break

        case '3' :
            sendPrintData([params.query.nome, params.query.cargo], ['local', 'Brother QL-803']) 
        break

        case '4' :
            sendPrintData([params.query.nome, params.query.cargo], ['local', 'Brother QL-810Wa']) 
        break
      }
        
        //sendPrintData(['Colgate', "Phillipe"], ['local', 'Brother QL-810W'])    
        /*asyncMail(data, (err, data) => {
          if(err) {
            //console.log("ERRO REJECT")
            reject("- VERIFIQUE A INTERNET");
          } else {
            resolve(data);
          }
        });*/
        resolve(data);

    });
  }

  async update (id, data, params) {
    return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
