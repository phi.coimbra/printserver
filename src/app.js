const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('winston');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');


const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');


const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));


// Set up Plugins and providers
app.configure(express.rest());

app.configure(socketio(function(io) {
    io.on('connection', function(socket, obj) {
      
      // send the players object to the new player
      // socket.emit('currentPlayers', players);
      // update all other players of the new player
      //socket.broadcast.emit('newPlayer', players[socket.id]);

      console.log('data:', socket.conn.id);  

      //socket.on('playerMovement', function (movementData) {
        //players[socket.id].x = movementData.x;
      //});

      socket.on('disconnect', function (data) {
        // remove this player from our players object
        //delete players[socket.id];
        // emit a message to all players to remove this player
        //io.emit('disconnect', socket.id);

      });
    });
    
    io.use(function (socket, next) {
        // Exposing a request property to services and hooks
        //socket.feathers.myid = socket.conn.id;
        next();
    });
}));

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

app.get('*', (req, res) => {
    //res.sendFile(path.join(__dirname, '/public', 'index.html'));
    res.sendFile( path.resolve('public/index.html') );
});

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;
