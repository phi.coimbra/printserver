const assert = require('assert');
const app = require('../../src/app');

describe('\'print\' service', () => {
  it('registered the service', () => {
    const service = app.service('print');

    assert.ok(service, 'Registered the service');
  });
});
